# firestore_missing_plugin_bug

This is a mini project that exemplifies a MissingPluginException I get when trying to run unit tests 
towards the firebase emulator.

## To reproduce the error
      
1. Clone this repo

1. Generate the json serialization support classes (*.g.dart)
    
        flutter packages pub run build_runner build --delete-conflicting-outputs

1. Try to execute the test in user_dao_test.dart
