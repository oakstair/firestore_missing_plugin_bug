import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firestore_missing_plugin_bug/model/entity/entity.dart';

abstract class EntityDao<E extends Entity> {

  String get name;

  DocumentReference document(String path) => FirebaseFirestore.instance.doc(path);
  CollectionReference get collection => FirebaseFirestore.instance.collection(name);

  E fromJson(Map<String, dynamic> json);

  Future<E> readById(String id, [bool deep = false]) async {
    return read('$name/$id', deep);
  }

  Future<E> read(String path, [bool deep = false]) async {
    DocumentSnapshot ds = await document(path).get();
    if (ds.exists) {
      E entity = fromJson(ds.data())..dbPath = ds.reference.path;
      if (entity != null && deep) {
        await loadSubCollections(entity);
      }
      return entity;
    } else {
      return null;
    }
  }

  Future<void> loadSubCollections(E entity) {
    return Future<void>.value();
  }

  Future<void> update(E e) {
    e.updatedAt = DateTime.now();
    return document(e.dbPath).set(jsonDecode(jsonEncode(e)));
  }

  Future<void> delete(E e) {
    return document(e.dbPath).delete();
  }

  Future<void> deleteByPath(String path) {
    return document(path).delete();
  }

}

abstract class TopEntityDao<E extends Entity> extends EntityDao<E> {

  Future<bool> create(E entity, [String id]) async {
    DocumentReference ref;
    if (id == null) {
      ref = await collection.add(entity.toJson());
    } else {
      ref = collection.doc(id);
      await ref.set(entity.toJson());
    }

    if (ref != null) {
      entity.dbPath = ref.path;
      return true;
    }
    else {
      return false;
    }
  }

  Future<List<E>> list([int limit = 100, bool deep = false]) async {
    try {
      QuerySnapshot qs = await collection.limit(limit).getDocuments();
      List<E> result = List();
      for (DocumentSnapshot ds in qs.docs) {
        E entity = fromJson(ds.data());
        if (entity != null) {
          entity.dbPath = ds.reference.path;
          if (deep) {
            await loadSubCollections(entity);
          }
          result.add(entity);
        } else {
          Type type = E;
          throw Exception('Failed to create an entity of type $type from: \n${ds.data}');
        }
      }

      return result;
    } catch (err) {
      print('Caught error: $err');
      throw err;
    }
  }


}

abstract class SubEntityDao<P extends Entity, E extends Entity> extends EntityDao<E> {

  Future<bool> create(P parent, E entity, [String id]) async {
    DocumentReference parentRef = document(parent.dbPath);

    DocumentReference ref;
    if (id == null) {
      ref = await parentRef.collection(name).add(entity.toJson());
    } else {
      ref = await parentRef.collection(name).doc(id);
      await ref.set(entity.toJson());
    }


    if (ref != null) {
      entity.dbPath = ref.path;
      return true;
    }
    else {
      return false;
    }
  }

  Future<List<E>> list(P parent, [bool deep = false]) async {

    try {
      DocumentReference parentRef = document(parent.dbPath);
      CollectionReference subCollection = parentRef.collection(name);
      QuerySnapshot qs = await subCollection.get();
      List<E> result = List();
      for (DocumentSnapshot ds in qs.docs) {
        E entity = fromJson(ds.data());
        if (entity != null) {
          entity.dbPath = ds.reference.path;
          if (deep) {
            await loadSubCollections(entity);
          }
          result.add(entity);
        } else {
          Type type = E;
          throw Exception('Failed to create an entity of type $type from: \n${ds.data}');
        }
      }

     return result;
    } catch (err) {
      print('Caught error: $err');
      throw err;
    }
  }


}
