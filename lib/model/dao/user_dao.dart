import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firestore_missing_plugin_bug/model/dao/base_dao.dart';
import 'package:firestore_missing_plugin_bug/model/entity/user.dart';

abstract class UserDao extends TopEntityDao<User> {
  UserDao();

  Future<User> findByEmail(String email);

}

class UserDaoImpl extends UserDao {
  UserDaoImpl();

  @override
  String get name => 'users';

  @override
  User fromJson(Map<String, dynamic> json) {
    return User.fromJson(json);
  }

  @override
  Future<User> findByEmail(String email) async {
    List<User> l = await _listByEmail(email);
    if (l.length == 0) {
      return null;
    } else if (l.length == 1) {
      return l[0];
    } else {
      throw Exception('Expected zero or one User but got ${l.length}. [email: $email]');
    }
  }

  Future<List<User>> _listByEmail(String email) async {
    return _listByFieldEqual('email', email);
  }

  Future<List<User>> _listByFieldEqual(String field, String value) async {
    try {
      QuerySnapshot qs = await collection.where(field, isEqualTo: value).getDocuments();
      List<User> result = List();
      for (DocumentSnapshot ds in qs.docs) {
        User user = User.fromJson(ds.data());
        if (user != null) {
          user.dbPath = ds.reference.path;
          result.add(user);
        } else {
          throw Exception('Failed to create User from: \n${ds.data}');
        }
      }
      return result;
    } catch (err) {
      print('Caught error: $err');
      throw err;
    }
  }
}
