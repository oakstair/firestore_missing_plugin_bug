// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..createdAt = json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String)
    ..country = json['country'] as String
    ..email = json['email'] as String
    ..name = json['name'] as String
    ..loginTimes = json['loginTimes'] as int
    ..lastLogin = json['lastLogin'] == null
        ? null
        : DateTime.parse(json['lastLogin'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'country': instance.country,
      'email': instance.email,
      'name': instance.name,
      'loginTimes': instance.loginTimes,
      'lastLogin': instance.lastLogin?.toIso8601String(),
    };
