
import 'dart:convert';

import 'package:firestore_missing_plugin_bug/model/entity/entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class User extends Entity implements Comparable<User> {
  String country; // E.g. SE
  String email;
  String name;
  int loginTimes;
  DateTime lastLogin;

  User();

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  factory User.fromJsonString(String json) => User.fromJson(jsonDecode(json));
  String toJsonString() => jsonEncode(this);

  @override
  String toString() => name;

  @override
  int compareTo(o) {
    return name.compareTo(o.name);
  }
}
