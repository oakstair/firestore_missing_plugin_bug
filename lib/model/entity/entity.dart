import 'package:json_annotation/json_annotation.dart';

/// Base entity
abstract class Entity<T> {
  @JsonKey(ignore: true)
  String dbPath;

  @JsonKey(ignore: true)
  bool get transient => dbPath == null;

  DateTime createdAt;
  DateTime updatedAt;

  Entity() {
    createdAt = DateTime.now();
    updatedAt = DateTime.now();
  }

  String get dbId {
    if (dbPath == null) {
      return null;
    } else {
      return dbPath.substring(dbPath.indexOf('/') + 1);
    }
  }

  factory Entity.fromJson(String json) {
    return null;
  }

  Map<String, dynamic> toJson();
}
