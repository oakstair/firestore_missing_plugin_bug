import 'package:firebase_core/firebase_core.dart';
import 'package:firestore_missing_plugin_bug/model/dao/user_dao.dart';
import 'package:firestore_missing_plugin_bug/model/entity/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';

/// TODO: Get firebase emulators to work an use them for unit testing dao's
void main() {

  UserDao dao;

  setUpAll(() async {
    WidgetsFlutterBinding.ensureInitialized();

    await Firebase.initializeApp();
    // 192.168.0.31
    // FirebaseFirestore.instance.settings = Settings(host: '0.0.0.0:8080', sslEnabled: false, persistenceEnabled: true);

    GetIt.I.registerSingleton<UserDao>(UserDaoImpl());
  });

  setUp(() {
    dao = GetIt.I<UserDao>();
  });

  test("UserDao.crud", () async {
    User t = User();
    t.name = 'Tezt';
    dao.create(t);

    var list = await dao.list(10);
    expect(list.length, 1);
  });

}
